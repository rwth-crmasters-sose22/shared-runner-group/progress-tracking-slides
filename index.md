---
title:  Concept Project DDP
description: Use speech to text function to transport information out of the construction site in the computer
author: Bora Karadeniz, Jaime Mozota
keywords: marp,marp-cli,slide
url: https://rwth-crmasters-sose22.gitlab.io/-/shared-runner-group/progress-tracking-slides/-/jobs/2467257951/artifacts/public/index.html
marp: true
image: https://j6f3f9w3.rocketcdn.me/wp-content/uploads/2020/08/Implementieren-Prozessanalyse.png

---

# Semi-automated communication path Project DDP

## Using voice messages to record processes 

---
<style>
img[alt~="center"], .center, .fence {
  display: block;
  margin: 0 auto;
}
kbd {
    background-color: #eee;
    border-radius: 3px;
    border: 1px solid #b4b4b4;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
    color: #333;
    display: inline-block;
    font-size: .85em;
    font-weight: 700;
    line-height: 1;
    padding: 2px 4px;
    white-space: nowrap;
}
.float-right {
    float: right;
}
.fence {
    display: flex;
    justify-content: center;
    align-items: center;
}
.fence img {
    width: auto;
    max-width: 100%;
    height: 55%;
    max-height: 55%;
}
.margin-top {
    margin-top: 1em;
}
.padding-top {
    padding-top: 1em;
}
</style>

# Collected stories

- _"I do not know what the status of the reinforcement work is, the concrete comes at 3 o'clock."_, said the construction manager in the container in front of :computer:  
- _"I do not know who damaged the insulation panels for the ceilings in the underground garage. There were 2 trades working on the ceiling.."_, said the construction manager in the container.
- _"I said to the crane driver to store the insulation boards in area X, why are they in area Y now. "_, said the construction manager on the :building_construction:

- _"Where is the material i am looking for "_, said :construction_worker: on the :building_construction:

---

>Problem 1) In a project I worked on for Leonhard Weiss, we built 2 schools. The buildings were about 150-200 m apart. Getting information or sending information to a construction site in front of the container is difficult. But even more from a/ to a construction that is not in sight.
  
>Problem 2) In a project also for LW we had 2 underground garages. Serious defects on the insulation panels were discovered during acceptance. Now both trades blamed each other. 

>Problem 3/4) The reinforcement and other materials were very often stored incorrectly. So a long time had to be searched for it or there were double orders. Also wrong material were built in in wrong places. 



---
::: fence

@startuml

@startmindmap

*[#Orange] Flow of information
** within the same organization
*** same buisness unit
**** face-to-face 
**** meeting
**** phone
**** e-mail
**** report/ note
**** chat
*** different unit
**** e-mail
**** phone
**** videoconference
**** face-to-face meeting
**** screen sharing
** across organizations
*** face-to-face meeting
*** phone
*** videoconference
*** e-mail
*** meeting
**[#Orange] with freelancers/individuals
***[#Orange] face-to-face
*** phone 
*** mail
***[#Orange] meeting
***[#Orange] walkie-talkie
***[#Orange] signs
***[#Green] audio message 

@endmindmap

@enduml
:::
---

### Commands  

Friedemann Schulz von Thun  

4 ear model  
 - facts  
 - self disclosure  
 - relation  
 - appeal  

--> command subcommand -a -b target

---

# Open-source & hardware secondary research

- Apple Watch or Google Pixel Watch

- [ifc open shell](https://github.com/IfcOpenShell/IfcOpenShell)  
➡️ helps user and software developers to work with the _**IFC file**_ format

- [deepSpeech](https://github.com/mozilla/DeepSpeech)  
➡️ _**speech to text**_ repositpory that creates a text file out of a speech message recorded and sended by a phone or a smartwatch

- [Jarvis Voice assistant](https://github.com/ggeop/Python-ai-assistant)  
➡️ _**voice commanding assistant**_ service in Python 3.8 It can recognize human speech 

- [DeepLcom Translator](https://github.com/DeepLcom/deepl-python)  
➡️ _**language translation**_ API that allows other computer programs to send texts and documents to DeepL's servers and receive high-quality translations

---

# Locked concept

- [x] Attemp to use a smartwatch to send audio commands to the computer in the container (wifi, mqtt)
- [x] In the computer, convert speech to text (or speech assistant) and use commands to create a new in-progress model IFC file for example.
- [ ] Adittionally commands can be use to notify different types of reports on te construction site. For example:  

    - documentation (blame function gitlab)
    - lists for defects, safety hazards and material
    - billing (using list for built-in elements or works done)

---

## Thank you for your attention 
